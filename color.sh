#!/bin/bash

export BLACK='\033[0;30m'
export DARK_GRAY='\033[1;30m'
export RED='\033[0;31m'
export LIGHT_RED='\033[1;31m'
export GREEN='\033[0;32m'
export LIGHT_GREEN='\033[1;32m'
export BROWN_ORANGE='\033[0;33m'
export YELLOW='\033[1;33m'
export BLUE='\033[0;34m'
export LIGHT_BLUE='\033[1;34m'
export PURPLE='\033[0;35m'
export LIGHT_PURPLE='\033[1;35m'
export CYAN='\033[0;36m'
export LIGHT_CYAN='\033[1;36m'
export LIGHT_GRAY='\033[0;37m'
export WHITE='\033[1;37m'
export CLEAR_COLOR='\033[0m'

function to_black {
	printf $BLACK
}
function to_dark_gray {
	printf $DARK_GRAY
}
function to_red {
	printf $RED
}
function to_light_red {
	printf $LIGHT_RED
}
function to_green {
	printf $GREEN
}
function to_light_green {
	printf $LIGHT_GREEN
}
function to_brown_orange {
	printf $BROWN_ORANGE
}
function to_yellow {
	printf $YELLOW
}
function to_blue {
	printf $BLUE
}
function to_light_blue {
	printf $LIGHT_BLUE
}
function to_purple {
	printf $PURPLE
}
function to_light_purple {
	printf $LIGHT_PURPLE
}
function to_cyan {
	printf $CYAN
}
function to_light_cyan {
	printf $LIGHT_CYAN
}
function to_light_gray {
	printf $LIGHT_GRAY
}
function to_white {
	printf $WHITE
}
function to_nc {
	printf $CLEAR_COLOR
}
