#!/bin/bash
source color.sh

CURRENT_INDENT=""
FRONT_TAB="    "
BACK_TAB="\b\b\b\b"
VIRTUAL_HOME=$HOME/SpiNNaker_virtualenv

to_light_blue
printf "${CURRENT_INDENT}PyNN developpement environnement setup\n"
CURRENT_INDENT="$CURRENT_INDENT$FRONT_TAB"
printf "${CURRENT_INDENT}Installation de python et pip\n"
to_light_gray

sudo apt-get update
sudo apt-get install -y python2.7-dev python-pip python-tk
# sudo apt-get install python2.7-dev

to_light_blue
printf "${CURRENT_INDENT}Installation de virtualenv\n"
to_light_gray

sudo pip install virtualenv

to_light_blue
printf "${CURRENT_INDENT}Setup virtual environnement IN ~/SpiNNaker_virtualenv\n"
CURRENT_INDENT="$CURRENT_INDENT$FRONT_TAB"
printf "${CURRENT_INDENT}Create virtualenv and activate it\n"
to_light_gray

virtualenv $VIRTUAL_HOME
source $VIRTUAL_HOME/bin/activate

to_light_blue
printf "${CURRENT_INDENT}Install python dependencies\n"
to_light_gray

pip install numpy==1.12 scipy lxml matplotlib

to_light_blue
printf "${CURRENT_INDENT}Install SpiNNaker\n"
to_light_gray

pip install "sPyNNaker == 3.0.0"
pip install "pyNN-SpiNNaker == 3.0.0"
pip install "sPyNNakerExternalDevicesPlugin == 3.0.0"
pip install "sPyNNakerExtraModelsPlugin == 3.0.0"
pip install "SpiNNakerGraphFrontEnd == 3.0.0"

to_light_blue
CURRENT_INDENT="$CURRENT_INDENT$BACK_TAB"
printf "${CURRENT_INDENT}Configure SpyNNaker and SpiNNakerGraphFrontEnd\n"
to_light_gray

python -c "import spynnaker.pyNN"
python -c "import spinnaker_graph_front_end"

deactivate

to_light_purple
cat $HOME/.spynnaker.cfg

to_brown_orange
printf "This is the END :D\n"
printf "\nStart the virtualenv with : $ source $VIRTUAL_HOME/bin/activate\n"
to_nc
