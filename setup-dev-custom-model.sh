#!/bin/bash
source color.sh

# Install spinn_common Library
wget https://github.com/SpiNNakerManchester/spinn_common/archive/3.0.0.tar.gz

tar -xvf 3.0.0.tar.gz

rm 3.0.0.tar.gz

cd spinn_common-3.0.0

make

make install

cd ..

rm -rf spinn_common-3.0.0

# Install SpiNNFrontEndCommon Library
wget https://github.com/SpiNNakerManchester/SpiNNFrontEndCommon/archive/3.0.0.tar.gz

tar -xvf 3.0.0.tar.gz

rm 3.0.0.tar.gz

cd SpiNNFrontEndCommon-3.0.0/c_common

make

make install

cd ../..

rm -rf SpiNNFrontEndCommon-3.0.0

# Get a SpiNNaker source installation
cd ~/.local/share

wget https://github.com/SpiNNakerManchester/sPyNNaker/archive/3.0.0.tar.gz

tar -xvf 3.0.0.tar.gz

rm 3.0.0.tar.gz

printf "export NEURAL_MODELLING_DIRS=$PWD/sPyNNaker-3.0.0/neural_modelling\n" >> ~/.bash_var
