#!/bin/bash
source color.sh

INITIAL_DIR=$PWD
ARM_C_COMPILER_INSTALL_DIR="$HOME/.local/share/ARM-DEV-TOOLS"
SPINNAKER_TOOLS_INSTALL_DIR="$HOME/.local/share/SPINNAKER-TOOLS"

to_light_blue
printf "Setup C developpement environnement for SpiNNaker\n\n"
printf "\t Install C developpement dependencies \n"
to_nc

sudo apt-get update
sudo apt-get install -y libc6-i386 libc6-dev-i386
sudo apt-get install -y perl perl-tk

echo y | sudo perl -MCPAN -e 'install String::CRC32'

to_light_blue
printf "\t Install ARM C Compiler\n"
to_nc

mkdir -p $ARM_C_COMPILER_INSTALL_DIR
cd $ARM_C_COMPILER_INSTALL_DIR

wget https://github.com/SpiNNakerManchester/SpiNNakerManchester.github.io/releases/download/v1.0-lin-dev/arm-2013.05.tgz
tar -xvf arm-2013.05.tgz --strip-components=1
rm arm-2013.05.tgz

printf "\n\nif [ -f ~/.bash_var ]; then\n\t. ~/.bash_var\nfi\n" >> ~/.bashrc
printf "export PATH=\$PATH:$ARM_C_COMPILER_INSTALL_DIR/bin\n" >> ~/.bash_var

to_light_blue
printf "\t Install SpiNNakerTools\n"
to_nc

mkdir -p $SPINNAKER_TOOLS_INSTALL_DIR
cd $SPINNAKER_TOOLS_INSTALL_DIR

wget https://github.com/SpiNNakerManchester/spinnaker_tools/releases/download/v3.0.1/spinnaker_tools_3.0.1.tar.gz
tar -xvf spinnaker_tools_3.0.1.tar.gz --strip-components=1
rm spinnaker_tools_3.0.1.tar.gz

printf "export PATH=\$PATH:$SPINNAKER_TOOLS_INSTALL_DIR/tools\n" >> ~/.bash_var
printf "export PERL5LIB=\$PERL5LIB:$SPINNAKER_TOOLS_INSTALL_DIR/tools\n" >> ~/.bash_var
printf "export SPINN_DIRS=$SPINNAKER_TOOLS_INSTALL_DIR\n" >> ~/.bash_var

source $HOME/.bash_var

make

to_yellow
printf "\n\n\nCongratulation the installation is done.\n\n"
to_red
printf "\tSee in the ~/.local/share/SPINNAKER-TOOLS/apps to see exemples\n"
to_nc
